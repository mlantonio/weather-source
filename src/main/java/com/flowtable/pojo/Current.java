package com.flowtable.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Current implements Serializable {
    String observationTime;
    String temperature;
    String weatherCode;
    Integer windSpeed;
    Integer windDegree;
    String windDir;
    Integer pressure;
    Integer humidity;
    Integer cloudCover;
    Integer feelsLike;
    Integer uvIndex;
    Integer visibility;
}
