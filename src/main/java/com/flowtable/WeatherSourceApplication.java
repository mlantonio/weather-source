package com.flowtable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.flowtable.pojo.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

@EnableBinding(Source.class)
@SpringBootApplication
public class WeatherSourceApplication{

	@Autowired
	WebClient.Builder webclient;

	@Value("${weather.api}")
	private String weatherApi;

	public static void main(String[] args) {
		SpringApplication.run(WeatherSourceApplication.class, args);
	}

	class WeatherDeserializer extends StdDeserializer<Current> {
		private static final long serialVersionUID = 1L;

		public WeatherDeserializer() {
			this(null);
		}
		public WeatherDeserializer(Class<?> vc) {
			super(vc);
		}

		@Override
		public Current deserialize(JsonParser jp, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			JsonNode node = jp.getCodec().readTree(jp);

			JsonNode currentNode = node.get("current");
			String observationTime = currentNode.get("observation_time").asText();
			String temperature = currentNode.get("temperature").asText();
			String weatherCode = currentNode.get("weather_code").asText();
			Integer windSpeed = currentNode.get("wind_speed").asInt();
			Integer windDegree = currentNode.get("wind_degree").asInt();
			String windDir = currentNode.get("wind_dir").asText();
			Integer pressure = currentNode.get("pressure").asInt();
			Integer humidity = currentNode.get("humidity").asInt();
			Integer cloudCover = currentNode.get("cloudcover").asInt();
			Integer feelsLike = currentNode.get("feelslike").asInt();
			Integer uvIndex = currentNode.get("uv_index").asInt();
			Integer visibility = currentNode.get("visibility").asInt();

			Current current = new Current(observationTime, temperature, weatherCode, windSpeed, windDegree,windDir, pressure, humidity, cloudCover, feelsLike, uvIndex, visibility);
			return current;
		}
	}

	@Bean
	@InboundChannelAdapter(value = Source.OUTPUT, poller = @Poller(fixedDelay = "30000", maxMessagesPerPoll = "1"))
	public MessageSource<Current> weatherMessageSource() throws JsonProcessingException {

		String url = "http://api.weatherstack.com/current" +
				"?access_key="+weatherApi+
				"&query=New York";

		String jsonResponse = webclient.build()
				.get()
				.uri(url)
				.retrieve()
				.bodyToMono(String.class)
				.block();

		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Current.class, new WeatherDeserializer());
		mapper.registerModule(module);
		Current current = mapper.readValue(jsonResponse, Current.class);

		return () -> MessageBuilder.withPayload(current).build();
	}
}
